#!/usr/bin/env python2.7

"""
Simple Mastodon client bot

Author: @wim_v12e

This is a simple example of a bot to show you how easy it is to create one. Please let me know if you have any questions. 
"""

"""
Import the function to create a Mastodon client object. 
This "Mastodon client object" is a collection of actions (called "methods") that you can perform when communicating with a Maston instance.
For a detailed list of all methods you can call, see https://mastodonpy.readthedocs.io/en/latest/#
Below I give a few simple examples.
"""
from mastodon_client import create_mastodon_client

instance='octodon.social'
botname='pcodebot'

"""
This function creates a Mastodon client object which is stored in `pcodebot`. If it's the first time you call it, it will prompt for your email and password.
The function also returns a dictonary (a labeled list of things) of info about the bot, `pcodebot_info`, see below.
"""
(pcodebot, pcodebot_info) = create_mastodon_client(instance, botname)

"""
1. Post a toot

With the `pcodebot` object, posting a toot is very simple:
"""
toot_str="Hello from the #pcode bot!"
pcodebot.toot(toot_str)

"""
If you want to see what you tooted:
"""    
#print(toot_str)

"""
2. Get a list of all messages with hashtag #pcode 
To get all toots with a given hashtag we use the `timeline_hashtag()` method. Other timelines are `timeline_home`, `timeline_local` and `timeline_public`.

The toot is actually a dictionary, the text that is displayed is stored under 'content'. To see the raw toot just say `print msg`
"""
practicecoding_msgs = pcodebot.timeline_hashtag('pcode')
for msg in practicecoding_msgs:    
    print msg['content'] 


"""
The dictionary `pcodebot_info` is where you put any information you want the bot to remember. 

To see what it contains, just print it:
"""    
#print pcodebot_info

"""
To add info, for example the number of toots in the public timeline:
"""    
#pcodebot_info['ntoots_public'] = len(public_timeline)

"""
If you changed info in pcodebot_info or added info to it, save the info to a file before exiting.
"""
#save_mastodon_client_info(pcodebot_info)
