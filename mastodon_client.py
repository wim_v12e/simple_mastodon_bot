# Author: @wim_v12e
"""
The functions below are used to set up a Mastodon client and its state. You should not have to change this to create your own bot.
"""
from mastodon import Mastodon
import json
import os

"""
Call this function with the name of the instance and the name you want for your bot.
It returns a Mastodon client object and a dictionary with the state for the object.
It creates three files:

     client_name+'_clientcred.txt'
     client_name+'_usercred.txt'
     client_name+'_state.json'

If you want a clean restart, just delete these files.
"""

def create_mastodon_client(instance, client_name):    

    # Register app - only once! 
    # To do this again, delete or move the file client_name+'_clientcred.txt'
    if not os.path.exists(client_name+'_clientcred.txt'):
        Mastodon.create_app(
            client_name,
            to_file = client_name+'_clientcred.txt',
            api_base_url='https://'+instance
        )
        email=raw_input('email? ')
        password=raw_input('password? ')        

        mastodon_client = Mastodon(client_id = client_name+'_clientcred.txt',api_base_url='https://'+instance)

        mastodon_client.log_in(
            email,
            password,
            to_file = client_name+'_usercred.txt'
        )

    # Save the state of the app in a JSON file
    if os.path.exists(client_name+'_state.json'):
        with open(client_name+'_state.json','r') as file:
            client_state=json.load(file)
    else:
        client_state={'since_id':0,'client_name':client_name}

    if 'instance' in client_state:
        instance=client_state['instance']
    else:
        client_state['instance'] = instance

    ## Create actual instance
    mastodon_client = Mastodon(
        client_id = client_name+'_clientcred.txt',
        access_token = client_name+'_usercred.txt',
        api_base_url='https://'+instance
    )
    return (mastodon_client, client_state)

"""
This function saves the client state to a JSON file. It will be read automatically on the next run of the bot.
"""
def save_mastodon_client_info(client_state):
    state_file_name = client_state['client_name']+'_state.json'
    with open(state_file_name,'w') as state_file:
        json.dump(client_state,state_file)

